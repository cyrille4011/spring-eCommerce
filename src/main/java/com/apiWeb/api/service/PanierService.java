package com.apiWeb.api.service;

import com.apiWeb.api.exception.NotFoundException;
import com.apiWeb.api.model.Panier;
import com.apiWeb.api.repo.PanierRep;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PanierService {
    private final PanierRep panierRep;

    public PanierService(PanierRep panierRep) {
        this.panierRep = panierRep;
    }

    public List<Panier> liste(){
        return panierRep.findAll();
    }

    public Panier listeById(Long id){
        return panierRep.findPanierById(id).orElseThrow(()-> new NotFoundException("panier " + id + "n'existe pas!!"));
    }

    public Panier AddPanier(Panier panier){
        return panierRep.save(panier);
    }

    public Panier EditPanier(Panier panier){
        return panierRep.save(panier);
    }

    public void DeletePanier(Long id){
        panierRep.deleteById(id);
    }

}
