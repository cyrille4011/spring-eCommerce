package com.apiWeb.api.service;

import com.apiWeb.api.exception.NotFoundException;
import com.apiWeb.api.model.Categorie;
import com.apiWeb.api.repo.CategorieRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategorieService {
    private final CategorieRepo categorieRepo;

    @Autowired
    public CategorieService(CategorieRepo categorieRepo) {
        this.categorieRepo = categorieRepo;
    }

    public Categorie CreateCategorie(Categorie categorie){
        return categorieRepo.save(categorie);
    }


    public List<Categorie> ReadCategorie(){
        return categorieRepo.findAll();
    }

    public Categorie findCategorieById(Long id){
        return categorieRepo.findCategorieById(id)
                .orElseThrow(()-> new NotFoundException("Categorie " + id + "n'existe pas!!"));
    }

    public Categorie UpdateCategorie(Categorie categorie){
        return categorieRepo.save(categorie);
    }

    public void DeleteCategorie(Long id){
            categorieRepo.deleteById(id);
    }

}
