package com.apiWeb.api.service;

import com.apiWeb.api.exception.NotFoundException;
import com.apiWeb.api.model.Client;
import com.apiWeb.api.repo.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClientService {

    private final ClientRepo clientRepo;

    @Autowired
    public ClientService(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    public Client addClient(Client client){
        return clientRepo.save(client);
    }

    public Client updateClient(Client client){
        return clientRepo.save(client);
    }

    public List<Client> ListeClient(){
        return clientRepo.findAll();
    }


    public Client findClientById(Long id){
        return clientRepo.findClientById(id)
                .orElseThrow(()-> new NotFoundException("Client " + id + "n'existe pas!!"));
    }

    public void DeleteClient(Long id){
        clientRepo.deleteById(id);
    }


}
