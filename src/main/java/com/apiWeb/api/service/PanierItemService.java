package com.apiWeb.api.service;

import com.apiWeb.api.model.PanierItem;
import com.apiWeb.api.repo.PanierItemRep;
import org.springframework.stereotype.Service;

@Service
public class PanierItemService  {
    private final PanierItemRep panierItemRep;

    public PanierItemService(PanierItemRep panierItemRep) {
        this.panierItemRep = panierItemRep;
    }


    public PanierItem save(PanierItem panierItem) {
        return panierItemRep.save(panierItem);
    }
}
