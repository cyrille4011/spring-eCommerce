package com.apiWeb.api.service;

import com.apiWeb.api.exception.NotFoundException;
import com.apiWeb.api.model.Client;
import com.apiWeb.api.model.Produit;
import com.apiWeb.api.repo.Produitrepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProduitService {

    private final Produitrepo produitrepo;

    public ProduitService(Produitrepo produitrepo) {
        this.produitrepo = produitrepo;
    }

    public Produit Add(Produit produit){
        return produitrepo.save(produit);
    }

    public Produit Edit(Produit produit){
        return produitrepo.save(produit);
    }

    public List<Produit> Select(){
       return produitrepo.findAll();
    }

    public Produit findProduitById(Long id){
        return produitrepo.findProduitById(id)
                .orElseThrow(()-> new NotFoundException("produit " + id + "n'existe pas!!"));
    }

    public void Delete(Long id){
        produitrepo.deleteById(id);
    }


}
