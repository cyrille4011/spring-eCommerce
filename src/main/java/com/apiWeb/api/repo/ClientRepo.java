package com.apiWeb.api.repo;

import com.apiWeb.api.model.Categorie;
import com.apiWeb.api.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@CrossOrigin("*")
public interface ClientRepo extends JpaRepository<Client, Long> {

    //void deleteClient(Long id);

    Optional<Client> findClientById(Long id);
}
