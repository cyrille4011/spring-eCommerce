package com.apiWeb.api.repo;

import com.apiWeb.api.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@CrossOrigin("*")
public interface Produitrepo extends JpaRepository<Produit,Long> {

    Optional<Produit> findProduitById(Long id);
}
