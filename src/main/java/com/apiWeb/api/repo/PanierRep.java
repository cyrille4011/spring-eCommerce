package com.apiWeb.api.repo;

import com.apiWeb.api.model.Client;
import com.apiWeb.api.model.Panier;
import com.apiWeb.api.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
public interface PanierRep extends JpaRepository<Panier,Long> {
    Optional<Panier> findPanierById(Long id);

    public List<Panier> findPanierByClient(Client client);
}
