package com.apiWeb.api.repo;

import com.apiWeb.api.model.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@CrossOrigin("*")
public interface CategorieRepo extends JpaRepository<Categorie,Long> {

    void deleteByCategorie(Long id);

    Optional<Categorie> findCategorieById(Long id);
}
