package com.apiWeb.api.repo;

import com.apiWeb.api.model.PanierItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
public interface PanierItemRep extends JpaRepository<PanierItem,Long> {
}
