package com.apiWeb.api.ressource;

import com.apiWeb.api.model.Categorie;
import com.apiWeb.api.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategorieRessource {

    private final CategorieService categorieService;

    @Autowired
    public CategorieRessource(CategorieService categorieService) {
        this.categorieService = categorieService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Categorie>> getAllCategorie(){
        List<Categorie> categories = categorieService.ReadCategorie();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Categorie> getAllCategorie(@PathVariable("id") Long id ){
        Categorie categorie = categorieService.findCategorieById(id);
        return new ResponseEntity<>(categorie, HttpStatus.OK);
    }

    @PutMapping("/add")
    public ResponseEntity<Categorie> AjouterCategorie(@RequestBody Categorie categorie){
        Categorie Newcategorie = categorieService.CreateCategorie(categorie);
        return new ResponseEntity<>(Newcategorie, HttpStatus.CREATED);
    }

    @PutMapping("/edit")
    public ResponseEntity<Categorie> ModifierCategorie(@RequestBody Categorie categorie){
        Categorie Editcategorie = categorieService.UpdateCategorie(categorie);
        return new ResponseEntity<>(Editcategorie, HttpStatus.CREATED);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> SupprimerCategorie(@PathVariable("id") Long id){
        categorieService.DeleteCategorie(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
