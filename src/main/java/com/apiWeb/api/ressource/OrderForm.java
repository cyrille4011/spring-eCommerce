package com.apiWeb.api.ressource;

import com.apiWeb.api.model.Client;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

public class OrderForm {
    private Client client=new Client();
    private List<PanierProduct> products=new ArrayList<>();

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<PanierProduct> getProducts() {
        return products;
    }

    public void setProducts(List<PanierProduct> products) {
        this.products = products;
    }
}

class PanierProduct{
    private Long id;
    private int quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}