package com.apiWeb.api.ressource;

import com.apiWeb.api.model.Client;
import com.apiWeb.api.model.Panier;
import com.apiWeb.api.model.PanierItem;
import com.apiWeb.api.model.Produit;
import com.apiWeb.api.repo.ClientRepo;
import com.apiWeb.api.repo.PanierRep;
import com.apiWeb.api.repo.Produitrepo;
import com.apiWeb.api.service.PanierItemService;
import com.apiWeb.api.service.PanierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/paniers")
public class PanierRessource<PanierItemRep> {

    private final PanierService panierService;

    private final PanierItemService panierItemService;

    @Autowired
    private Produitrepo productRepo;

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private PanierRep panierRep;


    public PanierRessource(PanierService panierService, PanierItemService panierItemService) {
        this.panierService = panierService;
        this.panierItemService = panierItemService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Panier>> ReadProduit(){
        List<Panier> Paniers = panierService.liste();
        return new ResponseEntity<>(Paniers , HttpStatus.OK);
    }

    @PostMapping("/Commande")
    public Panier saveOrder(@RequestBody OrderForm orderForm){
        Client client=new Client();
        client.setNom(orderForm.getClient().getNom());
        client.setPrenom(orderForm.getClient().getPrenom());
        client.setTel(orderForm.getClient().getTel());
        client.setAddress(orderForm.getClient().getAddress());
        client=clientRepo.save(client);
        System.out.println(client.getId());

        Panier panier=new Panier();
        panier.setClient(client);
        panier.setDate_panier(new Date());
        panier= panierRep.save(panier);
        double total=0;
        for(PanierProduct p:orderForm.getProducts()){
            PanierItem panierItem=new PanierItem();
            panierItem.setPanier(panier);
            Produit produit=productRepo.findProduitById(p.getId()).get();
            panierItem.setProduct(produit);
            panierItem.setPrice(produit.getPrix());
            panierItem.setQuantity(p.getQuantity());
            panierItemService.save(panierItem);
            total+=p.getQuantity()*produit.getPrix();
        }
        panier.setTotalAmount(total);
        return panierRep.save(panier);
    }

}
