package com.apiWeb.api.ressource;

import com.apiWeb.api.model.Categorie;
import com.apiWeb.api.model.Produit;
import com.apiWeb.api.repo.CategorieRepo;
import com.apiWeb.api.repo.Produitrepo;
import com.apiWeb.api.service.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produits")
public class ProduitRessource {

    private final ProduitService produitService;

    @Autowired
    CategorieRepo categorieRep;

    @Autowired
    Produitrepo produitRep;


    @Autowired
    public ProduitRessource(ProduitService produitService) {
        this.produitService = produitService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Produit>> ReadProduit(){
        List<Produit> produits = produitService.Select();
        return new ResponseEntity<>(produits , HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Produit> getProduit(@PathVariable("id") Long id){
        Produit produit = produitService.findProduitById(id);
        return new ResponseEntity<>(produit , HttpStatus.OK);
    }

    @PutMapping("/add")
    public ResponseEntity<Produit> Create(Produit produit){
        Produit Addproduit = produitService.Add(produit);
        return new ResponseEntity<>(Addproduit , HttpStatus.CREATED);
    }

    @PutMapping("/edit")
    public ResponseEntity<Produit> Update(Produit produit){
        Produit Editproduit = produitService.Edit(produit);
        return new ResponseEntity<>(Editproduit,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> Delete(Long id){
        produitService.Delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{produitId}/categorie/{categorieId}")
    Produit produitTocategorie(@PathVariable("id") Long produitId , @PathVariable("id") Long categorieId){
        Produit produit = produitRep.findProduitById(produitId).get();
        Categorie categorie = categorieRep.findCategorieById(categorieId).get();
        produit.asignCategorie(categorie);
        return produitRep.save(produit);
    }

}
