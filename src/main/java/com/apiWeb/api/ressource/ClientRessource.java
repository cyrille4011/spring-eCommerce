package com.apiWeb.api.ressource;

import com.apiWeb.api.model.Categorie;
import com.apiWeb.api.model.Client;
import com.apiWeb.api.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientRessource {

    private final ClientService clientService;

    @Autowired
    public ClientRessource(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Client>> getAllClient(){
        List<Client> clients = clientService.ListeClient();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Client> getAllClient(@PathVariable("id") Long id ){
        Client client = clientService.findClientById(id);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @PutMapping("/add")
    public ResponseEntity<Client> AjouterClient(@RequestBody Client client){
        Client Addclient = clientService.addClient(client);
        return new ResponseEntity<>(Addclient, HttpStatus.CREATED);
    }

    @PutMapping("/edit")
    public ResponseEntity<Client> ModifierClient(@RequestBody Client client){
        Client Editclient = clientService.updateClient(client);
        return new ResponseEntity<>(Editclient, HttpStatus.CREATED);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> SupprimerClient(@PathVariable("id") Long id){
        clientService.DeleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
