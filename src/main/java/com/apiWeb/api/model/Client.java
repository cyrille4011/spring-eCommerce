package com.apiWeb.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "clients")
public class Client implements Serializable {
    @Id
    @SequenceGenerator(
            name = "client_sequence",
            sequenceName = "client_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "client_sequence"
    )
    private Long id;

    @Column(
            name = "nom",
            nullable = false,
            columnDefinition="TEXT"
    )
    private String Nom;

    @Column(
            name= "prenom"
    )
    private String Prenom;

    @Column(
            name="date_naissance",
            columnDefinition = "DATE"
    )
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate DateNaissance;

    @Column(
            name = "tel"
    )
    private String Tel;

    @Column(
            name = "addresse"
    )
    private String Address;

    @JsonIgnore
    @OneToMany(mappedBy = "client")
    Set<Panier> paniers;

    public Client() {
    }

    public Client(String nom, String prenom, String tel, String address) {
        Nom = nom;
        Prenom = prenom;
        Tel = tel;
        Address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", Nom='" + Nom + '\'' +
                ", Prenom='" + Prenom + '\'' +
                ", Tel='" + Tel + '\'' +
                ", Address='" + Address + '\'' +
                '}';
    }
}
