package com.apiWeb.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categorie")
public class Categorie implements Serializable {

    @Id
    @SequenceGenerator(
            name = "cate_sequence",
            sequenceName = "cate_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cate_sequence")
    private Long id;
    @Column(
            name ="categorie",
            nullable = false,
            columnDefinition = "TEXT")
    private String categorie;

    @JsonIgnore
    @OneToMany(mappedBy = "categorie")
    private Set<Produit> produits = new HashSet<>();

    public Categorie() {
    }

    public Categorie(String categorie) {
        this.id = id;
        this.categorie = categorie;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public Set<Produit> getProduits() {
        return produits;
    }

    @Override
    public String toString() {
        return "Categorie{" +
                "id=" + id +
                ", categorie='" + categorie + '\'' +
                '}';
    }
}
