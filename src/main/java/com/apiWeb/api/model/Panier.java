package com.apiWeb.api.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Panier {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "panier")
    private Collection<PanierItem> PanierItems;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    private Integer quantite;

    private double totalAmount;

    private Date date_panier;

    public Panier() {
    }

    public Panier(Long id, Collection<PanierItem> panierItems, Client client, Integer quantite, double totalAmount, Date date_panier) {
        this.id = id;
        PanierItems = panierItems;
        this.client = client;
        this.quantite = quantite;
        this.totalAmount = totalAmount;
        this.date_panier = date_panier;
    }

    public Collection<PanierItem> getPanierItems() {
        return PanierItems;
    }

    public void setPanierItems(Collection<PanierItem> panierItems) {
        PanierItems = panierItems;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Date getDate_panier() {
        return date_panier;
    }

    public void setDate_panier(Date date_panier) {
        this.date_panier = date_panier;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }





}
