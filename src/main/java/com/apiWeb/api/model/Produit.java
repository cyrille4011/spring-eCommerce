package com.apiWeb.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.beans.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "produit")
public class Produit implements Serializable {

    @Id
    @SequenceGenerator(
            name = "produit_sequence",
            sequenceName = "produit_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "produit_sequence"
    )
    private Long id;

    @ManyToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "categorie_id",
            referencedColumnName = "id"
    )
    private Categorie categorie;



    @Column(name = "libelle",nullable = false)
    private String libelle;

    @Column(name = "details",columnDefinition = "TEXT")
    private String details;

    @Column(name = "prix",columnDefinition = "INTEGER")
    private Integer prix;

    @Column(name = "image",columnDefinition = "TEXT")
    private String imageUrls;

    @Column(name = "rating",columnDefinition = "INTEGER")
    private Integer rating;


    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Produit() {

    }

    public Produit(String libelle, Categorie categorie, String details, Integer prix, String imageUrls, Integer rating) {
        this.libelle = libelle;
        this.categorie = categorie;
        this.details = details;
        this.prix = prix;
        this.imageUrls = imageUrls;
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public String getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String imageUrls) {
        this.imageUrls = imageUrls;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", categorie=" + categorie +
                ", libelle='" + libelle + '\'' +
                ", details='" + details + '\'' +
                ", prix=" + prix +
                ", imageUrls='" + imageUrls + '\'' +
                ", rating=" + rating +
                '}';
    }

    public void asignCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}
